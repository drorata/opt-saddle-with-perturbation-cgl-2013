%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Presentation for the 3rd workshop of the CGL project
% (http://cgl.uni-jena.de/)
%
% For correct compilation you should have the file
% beamerthemegray.sty in the search path of TeX. You can find
% this file in my repository:
% https://github.com/drorata/graybeamer
%
% Author: Dror Atariah,
%         drorata@gmail.com, dror.atariah@fu-berlin.de
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass{beamer}

\IfFileExists{beamerthemegray.sty}{}{%
  \typeout{---===!!!!===---}
  \typeout{!!!Fatal Error: Get the gray theme from}
  \typeout{git@github.com:drorata/graybeamer.git and see the README!}
  \typeout{---===!!!!===---}
  \stop
}

\usetheme[pagenumbers]{gray}
\usepackage{xifthen}
\usepackage{tikz,pgfplots}
\usetikzlibrary{calc,decorations.markings,intersections}
\usetikzlibrary{external}
\tikzexternalize[prefix=tikz-cache/]
% \usepackage{animate}

\input{support}

\logosmall{logos/fu_small_logo}
\logobig{logos/fu_big_logo}
\logoaux{logos/cglLogo}
\logoauxx[1.cm]{logos/bms-logo-with-text}
\titleimage[2.92cm]{figures/title-image}

\title[Optimal Triangulation] % (optional, use only with long paper titles)
{Improving Optimal Triangulation of Saddle Surfaces}
% Optional subtitle
\subtitle{\tiny{\emph{Computational Geometric Learning} supported by FET-Open grant}}

\author[A.R.W.] % (optional, use only with lots of authors)
{D. Atariah \and G. Rote \and M. Wintraecken}
% - Give the names in the same order as the appear in the paper.

\institute[FU Berlin] % (optional, but mostly needed)
{Freie Universität Berlin}

\date[3\textsuperscript{rd} CGL Workshop] % (optional, should be abbreviation of conference name)
{CGL Workshop, 1.10.2013}
% - Either use conference name or its abbreviation.
% - Not really informative to the audience, more for people (including
% yourself) who are reading the slides online

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
% \AtBeginSection[]
% {
%   \begin{frame}<beamer>{Outline}
%     \tableofcontents[currentsection]
%   \end{frame}
% }
% \AtBeginSubsection[]
% {
%   \begin{frame}<beamer>{Outline}
%     \tableofcontents[currentsubsection]
%   \end{frame}
% }

\begin{document}

\begin{frame}[plain]
  \titlepage
\end{frame}

\section{Introduction}

\begin{frame}
  \frametitle{Motivation}
  \begin{minipage}{.7\linewidth}
    \begin{itemize}
    \item Using Taylor expansion every smooth surface is, locally, approximated by a quadratic patch
    \item Using Euclidean motions, the quadratic patches can be transformed to graphs of bi-variate polynomials
    \item So, lets approximate quadratic \emph{graphs}!
      %
      \[%
      \left\{\, (x,y,z) : z = F(x,y) \,\right\}
      \]%
    \end{itemize}
  \end{minipage}
  \hfill
  \begin{minipage}{.25\linewidth}
    \begin{center}
      \includegraphics[width=\linewidth]{%
        ./figures/full-contact-surface}
    \end{center}
  \end{minipage}
\end{frame}

\begin{frame}<beamer>{Outline}
  \tableofcontents
\end{frame}

\begin{frame}
  \frametitle{Vertical Distance}
  \begin{itemize}
  \item We are interested, w.l.o.g, in a neighborhood of the origin
  \item In this case the normal points upwards, and the following can approximate the Hausdorff distance
  \end{itemize}

  \begin{definition}[Vertical Distance]
    Given two domains \(D_1, D_2 \subset \R^2\) and two graphs \(f\colon D_1 \to \R\) and \(g\colon D_2 \to \R\) then the \emph{vertical distance} is%
    \[
    \dist[V]{f}{g} = \max_{(x,y)\in D_1 \cap D_2}|f(x,y)-g(x,y)|.
    \]
  \end{definition}
\end{frame}

\begin{frame}
  \frametitle{Some Properties of V-Dist}
  \begin{minipage}{.65\linewidth}
    \begin{lemma}
      Let \(A,B \subset \R^3\) be two sets such that their projection
      to the plane is identical.  Then the following holds %
      \[%
      \dist[H]{A}{B} \leq \dist[V]{A}{B}
      \]%
    \end{lemma}
  \end{minipage}
  \hfill
  \begin{minipage}{.3\linewidth}
    \begin{center}
      \input{./figures/v-dist-slope-badness}
    \end{center}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Some Properties of V-Dist (cont.)}
  \begin{lemma}[Every two points are the same]
    For every point \(p\in S\), there exists an affine transformation
    \(\mathcal{T}_p: \R^3 \to \R^3\) which satisfies the following:
    \begin{itemize}
    \item \(\mathcal{T}_p (p) = \vec{0}\)
    \item \(\mathcal{T}_p (S) = \tilde{S}\) which is a quadratic graph given by a polynomial of the form \(\tilde{F}(x,y) = a_1 x^2 + 2 a_2 x y + a_3 y^2 \)
    \item \(\forall q,r \in \R^3\) which lie on a vertical line we have%
      \[%
      |q-r| = |\mathcal{T}_p(q)-\mathcal{T}_p(r)|.
      \]%
    \end{itemize}
  \end{lemma}
\end{frame}

\begin{frame}
  \frametitle{Some Properties of V-Dist (cont.)}
  \begin{lemma}
    Given two points \(p,q\) on a quadratic graph \(S\) then %
    \[%
    \dist[V]{\ell_{pq}}{S} = \frac{1}{4} \left| \tilde{F}(p_x-q_x, p_y-q_y) \right|
    \]%
    where:
    \begin{itemize}
    \item \( \ell_{pq} \) is the line segment connecting \(p\) and \(q\)
    \item \(\tilde{F}(x,y)\) is the bi-variate polynomial
    \item V-dist is attained at the midpoint
    \end{itemize}
  \end{lemma}
\end{frame}

\section{Interpolating Approximation}

\begin{frame}
  \frametitle{Getting Started}

  \begin{center}
    For the sake of simplicity, from now on \(S = \left\{\, (x,y,z) : z = x y \,\right\} \)
  \end{center}

  \begin{block}{Goal}
    Find a triangle \(T\) with vertices \(p_0,p_1,p_2 \in S\) of \emph{maximal area} such that%
    \[\dist[V]{T}{S} \leq \epsilon\]%
    for some \(\epsilon>0\).
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{Optimize the Area of Planar Triangles}
  \begin{center}
    \only<1-5>{%
      \input{./figures/construct_optimal_triangle_rect_saddle}%
    }
  \end{center}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Optimize the Shape of Planar Triangles}

  Once optimizing the shape of the triangles of maximal area we obtain the following:

  \begin{center}
    \only<1-2>{
      \input{./figures/original-optimal-planar-hexagon}
    }
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Triangulate the Saddle}
  Project the planar triangulation to the surface
  \begin{center}
    \input{./figures/rect-saddle-from-plane-2-space.tex}
  \end{center}
\end{frame}

\section{Non-interpolating Approximation}

\begin{frame}
  \frametitle{Can We Do Better?}

  \begin{block}{What do we have?}
    Given an \(\epsilon>0\) and a saddle surface \(S\), we can find a family \(\mathcal{T}\) of triangles which interpolate the surface and
    \begin{itemize}
    \item have maximal area,
    \item optimal shape and
    \item maintain \(\dist[V]{S}{T} = \epsilon\) for all \(T \in \mathcal{T}\).
    \end{itemize}
  \end{block}
  \pause
  \begin{alertblock}{Question\dots}
    Can this be improved by allowing \emph{non-interpolating} triangles?
  \end{alertblock}
\end{frame}

\begin{frame}[fragile]
  \frametitle{In the Plane}
  \begin{block}{Fact}
    The area of the (interpolating) optimal triangles in the plane is \(2\sqrt{5} \epsilon\).%
  \end{block}

  \begin{minipage}{.55\linewidth}
    \begin{itemize}
    \item<8-> Obtain one parameter family of area preserving triangles
    \item<9> How should they be lifted?
    \end{itemize}
  \end{minipage}
  \hfill
  \begin{minipage}{.4\linewidth}
    \begin{center}
      \only<1-9>{ \input{./figures/area-preserving-triangles-animated}}
      % It is possible, using the animate package to animate the
      % sequence of images. Works only in adobe.
      % \animategraphics[loop,autoplay]{5}{./tmp/main-figure}{11}{18}
    \end{center}
  \end{minipage}
\end{frame}

% Start from 8 to reuse the TikZ from the previous slide and start from its
% last state.
\begin{frame}<8->
  \frametitle{Vertical Perturbed Projection}
  \vspace{2ex}
  \begin{columns}
    \column{.55\textwidth}
    \begin{minipage}[c][.6\textheight][c]{\linewidth}
      \begin{itemize}
      \item<8-> Find vertical lifting \(P_i(\xi,\eta)\) of \(p_i(\xi,\eta)\)
        such that \[\dist[V]{S}{\triangle P(\xi,\eta)}\] will be minimized
      \item<9-> Let \(\triangle P_\alpha(\xi,\eta)\) be the projected triangle with vertices on%
        \[S_\alpha = \left\{\,(x,y,z) : z=x y +\alpha \,\right\} \]%
      \item<10-> Vertical distance is attained at midpoints
      \end{itemize}
    \end{minipage}
    \column{.4\textwidth}
    \only<8->{\input{./figures/area-preserving-triangles-animated}}
  \end{columns}
\end{frame}


\begin{frame}<8->
  \frametitle{Vertical Perturbed Projection (Cont.)}
  \vspace{2ex}
  \begin{columns}
    \column{.55\textwidth}
    \begin{minipage}[c][.6\textheight][c]{\linewidth}
      \begin{itemize}
      \item<8-> Vertical distances from edges to \(S\) are %
        \[%
        \begin{aligned}
          & \frac{\xi \eta}{4}+ \alpha > 0 \\
          & \frac{1}{4}(\xi - \eta)^2 - \alpha > 0
        \end{aligned}
        \]%
        and has to be the same
      \item<9-> Therefore %
        \[%
        \alpha = \frac{1}{8} (\xi^2 - 3 \xi \eta + \eta^2)
        \]%
      \end{itemize}
    \end{minipage}
    \column{.4\textwidth}
    \only<8->{\input{./figures/area-preserving-triangles-animated}}
  \end{columns}
\end{frame}

\begin{frame}<8->
  \frametitle{Vertical Perturbed Projection (Cont.)}
  \vspace{2ex}
  \begin{columns}
    \column{.55\textwidth}
    \begin{minipage}[c][.6\textheight][c]{\linewidth}
      \vspace{1ex}
      \begin{itemize}
      \item<8-> The vertical distance is %
        \[\dist[V]{S}{\triangle P_\alpha(\xi)} = \left| \frac{1}{8} (\xi^2 -\xi \eta +\eta^2) \right|\]
        and its minimum can be found
      % \item<9-> Find the minimum of \(\dist[V]{S}{\triangle P_\alpha(\xi,\eta)}\)
      \item<9-> Min is attained for %
        \[
          \xi_0 = \sqrt{2\sqrt{5}\epsilon  \frac{2+\sqrt{3}}{\sqrt{3}}}
        \]
      \item<10-> And in this case %
        \[%
        \dist[V]{S}{\triangle P_\alpha(\xi_0)} = \frac{\sqrt{15}}{4} \epsilon \approx 0.968246 \epsilon
        \]%
      \end{itemize}
    \end{minipage}
    \column{.4\textwidth}
    \only<8->{\input{./figures/area-preserving-triangles-animated}}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Picture in Space}
  We can finally plot a non-interpolating optimal triangle which approximates a saddle surface
  \begin{center}
    \includegraphics[width=1\linewidth]{%
      ./figures/inter+non-inter-triangles-in-space}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{The Planar Super-Optimal Triangle}
  \begin{columns}
    \column{.55\textwidth}
    \begin{minipage}[c][.6\textheight][c]{\linewidth}
      \begin{itemize}
      \item<1-> Note the tangency property
      \item<2-> Super-optimal triangle is \emph{equilateral!}
      \item<3-> Recall they are projected to an offset of \(S\)
      \end{itemize}
      %
      \onslide<4->{
        \begin{center}
          \large{Thank you for your attention!}
        \end{center}
      }
    \end{minipage}
    \column{.4\textwidth}
    \only<1->{
      \begin{center}
        \input{./figures/optimal-non-interpolating-hexagon}
      \end{center}
    }
  \end{columns}
\end{frame}


\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
